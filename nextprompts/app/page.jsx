import Feed from "@components/Feed";

const Home = () => {
  return (
    <section className="w-full flex-center flex-col">
      <h1 className="text-center head_text">
        Discover & Share
        <br className="max-md:hidden" />
        <span className="text-center orange_gradient">AI powered Prompts</span>
      </h1>
      <p className="desc text-center">
        Prompts is an open-source AI prompting tool for modern world to
        discover, create and share creative prompts
      </p>

      {/* feed component  */}
      <Feed />
      {/* feed component  */}
    </section>
  );
};

export default Home;
