import NextAuth from "next-auth";
import GoogleProvider from "next-auth/providers/google";
import { connection } from "@utils/db";
import User from "@models/userSchema";

const handler = NextAuth({
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    }),
  ],
  async session({ session }) {
    const sessionuser = await User.findOne({
      email: session.user.email,
    });

    session.user.id = sessionuser._id.tostring();
    return session;
  },
  async signIn({ profile }) {
    try {
      await connection();
      const user = await User.findOne({
        email: profile.email,
      });
      if (!user) {
        await User.create({
          email: profile.email,
          username: profile.name.replace(" ", "").toLowerCase(),
          img: profile.picture,
        });
      }
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  },
});

export { handler as GET, handler as POST };
