import mongoose from "mongoose";

let isConnected = false;

export const connection = async () => {
  try {
    mongoose.set("strictQuery", true);

    if (isConnected) {
      console.log("MongoDB is already connected");
      return;
    }

    await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    isConnected = true;
    console.log("Database connected successfully");
  } catch (error) {
    console.log("error while connectin to database", error);
  }
};
