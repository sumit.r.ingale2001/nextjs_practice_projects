import Link from "next/link";
import React from "react";

const Navbar = () => {
  return (
    <div className="bg-black text-white flex items-center gap-8 p-4 navbar w-full">
      <Link href="/">All</Link>
      <Link href="/add">Add User</Link>
    </div>
  );
};

export default Navbar;
