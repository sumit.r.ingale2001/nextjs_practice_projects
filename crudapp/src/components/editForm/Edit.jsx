"use client";

import axios from "axios";
import { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";

const Form = ({ id }) => {
  const initialValues = {
    name: "",
    phone: "",
    email: "",
    username: "",
  };

  const [formValues, setFormValues] = useState(initialValues);
  const router = useRouter();

  const handleChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    const getUser = async () => {
      const {
        data: { data },
      } = await axios.get(`/api/user/getUser/${id}`);
      setFormValues(data);
    };
    getUser();
  }, [id]);

  const handleEdit = async (e) => {
    e.preventDefault();
    const { data } = await axios.put(`/api/user/editUser/${id}`, formValues);
    if (data?.success) {
      toast.success(data.message);
      router.push("/");
    } else {
      return toast.error(data.message);
    }
  };

  return (
    <>
      <form
        onSubmit={handleEdit}
        className="py-6 px-8 bg-white rounded-sm form flex flex-col gap-4"
      >
        <h3 className="text-3xl font-semibold text-center">Edit</h3>
        <input
          placeholder="Name"
          className="outline-none focus:outline-none focus:border-blue-500 rounded-md border border-gray-300 p-4 w-full"
          name="name"
          onChange={handleChange}
          autoComplete="off"
          value={formValues?.name}
        />
        <input
          placeholder="Phone"
          className="outline-none focus:outline-none focus:border-blue-500 rounded-md border border-gray-300 p-4 w-full"
          name="phone"
          onChange={handleChange}
          autoComplete="off"
          value={formValues?.phone}
        />
        <input
          placeholder="Email"
          className="outline-none focus:outline-none focus:border-blue-500 rounded-md border border-gray-300 p-4 w-full"
          name="email"
          onChange={handleChange}
          autoComplete="off"
          value={formValues?.email}
        />
        <input
          placeholder="Username"
          className="outline-none focus:outline-none focus:border-blue-500 rounded-md border border-gray-300 p-4 w-full"
          name="username"
          onChange={handleChange}
          autoComplete="off"
          value={formValues?.username}
        />

        <button
          type="submit"
          className="bg-blue-500 text-white p-3 rounded-full"
        >
          Edit
        </button>
      </form>
    </>
  );
};

export default Form;
