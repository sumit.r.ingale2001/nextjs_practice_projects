"use client";

import axios from "axios";
import { useState } from "react";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";

const Form = () => {
  const initialValues = {
    name: "",
    phone: "",
    email: "",
    username: "",
  };

  const [formValues, setFormValues] = useState(initialValues);
  const router = useRouter();

  const handleChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  const handleAdd = async (e) => {
    e.preventDefault();

    try {
      const { data } = await axios.post("/api/user/add", formValues);
      if (data) {
        if (data.success) {
          toast.success(data.message);
          router.push("/");
          setFormValues(initialValues);
          return;
        } else {
          return toast.error(data.message);
        }
      }
    } catch (error) {
      toast.error(error.message);
    }
  };

  return (
    <>
      <form
        onSubmit={handleAdd}
        className="py-6 px-8 bg-white rounded-sm form flex flex-col gap-4"
      >
        <h3 className="text-3xl font-semibold text-center">Add</h3>
        <input
          placeholder="Name"
          className="outline-none focus:outline-none focus:border-blue-500 rounded-md border border-gray-300 p-4 w-full"
          name="name"
          onChange={handleChange}
          autoComplete="off"
        />
        <input
          placeholder="Phone"
          className="outline-none focus:outline-none focus:border-blue-500 rounded-md border border-gray-300 p-4 w-full"
          name="phone"
          onChange={handleChange}
          autoComplete="off"
        />
        <input
          placeholder="Email"
          className="outline-none focus:outline-none focus:border-blue-500 rounded-md border border-gray-300 p-4 w-full"
          name="email"
          onChange={handleChange}
          autoComplete="off"
        />
        <input
          placeholder="Username"
          className="outline-none focus:outline-none focus:border-blue-500 rounded-md border border-gray-300 p-4 w-full"
          name="username"
          onChange={handleChange}
          autoComplete="off"
        />

        <button
          type="submit"
          className="bg-blue-500 text-white p-3 rounded-full"
        >
          Add
        </button>
      </form>
    </>
  );
};

export default Form;
