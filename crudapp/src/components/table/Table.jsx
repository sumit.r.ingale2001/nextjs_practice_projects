"use client";

import axios from "axios";
import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import toast from "react-hot-toast";

const Table = () => {
  const [userData, setUserData] = useState(null);
  const router = useRouter();

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const {
      data: { data },
    } = await axios.get("/api/user/getAllUsers");
    setUserData(data);
  };

  const deleteUser = async (id) => {
    const { data } = await axios.delete(`/api/user/deleteUser/${id}`);
    try {
      if (data.success) {
        toast.success(data.message);
        getUsers();
      } else {
        return toast.error(data.message);
      }
    } catch (error) {
      console.log(error, "error while calling delete user api");
    }
  };

  if (userData?.length < 1)
    return (
      <h1 className="mt-20 font-semibold text-gray-800 text-center">
        No users added...{" "}
        <button className="bg-blue-500 cursor-pointer hover:bg-blue-600 rounded-sm px-10 py-2 text-white"  onClick={() => router.push("/add")}>Add</button>
      </h1>
    );

  return (
    <table className="mt-16 border border-gray-400 table rounded-lg overflow-hidden">
      <thead>
        <tr className="bg-black text-white">
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Username</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {userData?.map((user) => (
          <tr key={user._id}>
            <td>{user.name}</td>
            <td>{user.email}</td>
            <td>{user.phone}</td>
            <td>{user.username}</td>
            <td>
              <button
                onClick={() => router.push(`/${user._id}`)}
                className="bg-blue-500 py-2 px-3 text-white rounded-sm cursor-pointer hover:bg-blue-600 "
              >
                Edit
              </button>
            </td>
            <td>
              <button
                onClick={() => deleteUser(user._id)}
                className="bg-red-500 py-2 px-3 text-white rounded-sm cursor-pointer hover:bg-red-600"
              >
                Delete
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
