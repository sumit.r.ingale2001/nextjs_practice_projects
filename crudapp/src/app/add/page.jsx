import Form from "@/components/form/Form";

const All = () => {
  return (
    <>
      <div className="min-h-screen flex justify-center items-center bg-gray-50">
        <Form />
      </div>
    </>
  );
};

export default All;

export const metadata = {
  title: "Add User",
  description: "This is the add user home page",
};
