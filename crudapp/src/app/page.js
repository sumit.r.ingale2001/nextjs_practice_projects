import Table from "@/components/table/Table";
import React from "react";

const Home = () => {
  return (
    <div>
      <Table />
    </div>
  );
};

export default Home;

export const metadata = {
  title: "All users",
  description: "This is an all users page",
};
