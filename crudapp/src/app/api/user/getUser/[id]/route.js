import connection from "@/database/db";
import User from "@/models/userSchema";
import { NextResponse } from "next/server";

export const GET = async (req, res) => {
  await connection();
  try {
    const id = req.url.split("/getUser/")[1]
    const user = await User.findById(id);
    if (!user)
      return NextResponse.json({ success: false, message: "User not found" });

    return NextResponse.json({
      success: true,
      message: "User found successfully",
      data: user,
    });
  } catch (error) {
    return NextResponse.json({
      success: false,
      message: "Error while getting the user",
      errorMessage: error,
    });
  }
};
