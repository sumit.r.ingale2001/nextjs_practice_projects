import User from "@/models/userSchema";
import { NextResponse } from "next/server";

export const PUT = async (req, res) => {
  try {
    const id = req.url.split("/editUser/")[1];
    const data = await req.json();
    const user = await User.findById(id);
    if (!user) {
      return NextResponse.json({ success: false, message: "User not found" });
    } else {
      await User.findByIdAndUpdate(user._id, data);
      return NextResponse.json({
        success: true,
        message: "User updated successfully",
      });
    }
  } catch (error) {
    console.log(error);
    return new Response(
      JSON.stringify({ data: false, message: "Error while editing the user" })
    );
  }
};
