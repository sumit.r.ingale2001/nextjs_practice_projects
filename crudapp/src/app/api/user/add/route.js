import connection from "@/database/db";
import User from "@/models/userSchema";

export const POST = async (NextRequest) => {
  await connection();
  try {
    const data = await NextRequest.json();
    const { name, phone, email, username } = data;
    if (!name || !phone || !email || !username) {
      return new Response(
        JSON.stringify({ success: false, message: "Fields cannot be empty" })
      );
    }

    const newUser = new User({
      name,
      phone,
      email,
      username,
    });

    await newUser.save();
    return new Response(
      JSON.stringify({ success: true, message: "Added successfully" })
    );
  } catch (error) {
    return new Response(
      JSON.stringify({
        success: false,
        message: "Something went wrong while adding user",
      })
    );
  }
};
