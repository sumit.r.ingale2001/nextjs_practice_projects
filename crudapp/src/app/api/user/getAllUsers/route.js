import connection from "@/database/db";
import User from "@/models/userSchema";

export const GET = async (NextRequest) => {
  try {
    await connection();
    const data = await User.find();
    return new Response(
      JSON.stringify({ success: true, message: "All users found", data })
    );
  } catch (error) {
    return new Response(
      JSON.stringify({
        success: false,
        message: "Something went wrong while getting all users",
      })
    );
  }
};
