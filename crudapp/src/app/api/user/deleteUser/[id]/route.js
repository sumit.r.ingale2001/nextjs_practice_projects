import connection from "@/database/db";
import User from "@/models/userSchema.js";
import { NextResponse } from "next/server";

export const DELETE = async (req, res) => {
  try {
    await connection()
    const id = req.url.split("/deleteUser/")[1];
    const user = await User.findById(id);
    if (!user) {
      return NextResponse.json({ success: false, message: "User not found" });
    } else {
      await User.findByIdAndDelete(user._id);
      return NextResponse.json({
        success: true,
        message: "User deleted successfully",
      });
    }
  } catch (error) {
    console.log(error);
    return NextResponse.json({
      success: false,
      message: "User deleted successfully",
    });
  }
};
