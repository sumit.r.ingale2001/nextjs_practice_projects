import Edit from "@/components/editForm/Edit";

const page = ({ params: { id } }) => {
  return (
    <div className="min-h-screen flex justify-center items-center bg-gray-50">
      <Edit id={id} />
    </div>
  );
};

export default page;

export const metadata = {
  title: "Edit",
  description: "This is the edit page",
};
