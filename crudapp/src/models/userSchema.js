import mongoose from "mongoose";

const schema = new mongoose.Schema({
  name: String,
  phone: String,
  email: String,
  username: String,
});

const User = mongoose.models.user || mongoose.model("user", schema);
export default User;