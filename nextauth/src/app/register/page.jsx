import RegisterForm from "../components/RegisterForm";

const Register = () => {
    return (
        <>
            <div className="min-h-screen bg-gray-100 flex flex-col justify-center items-center">
                <RegisterForm />
            </div>
        </>
    );
};

export default Register;

export const metadata = {
    title: "Register",
    description: "Register page of the nextAuth app",
};
