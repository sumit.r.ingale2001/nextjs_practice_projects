import Logout from "../components/Logout";

const HomePage = () => {
    return (
        <div className="min-h-screen bg-gray-100 flex justify-center items-center">
            <div className="bg-white py-10 px-10 rounded-sm">
                <h3 className="text-2xl uppercase text-center mb-3 font-semibold">
                    welcome to home page
                </h3>
                <p className="mb-3 first-letter:text-center">
                    This is just a personalized homepage for nextAuth
                    application
                </p>
                <Logout />
            </div>
        </div>
    );
};

export default HomePage;

export const metadata = {
    title: "Home",
    description: "Home page of nextAuth app",
};
