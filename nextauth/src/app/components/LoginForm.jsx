"use client";

import Link from "next/link";
import { Input } from "./input";
import { useState } from "react";
import axios from "axios";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";

const LoginForm = () => {
    const defaultValue = {
        email: "",
        password: "",
    };

    const [formValues, setFormValues] = useState(defaultValue);

    const router = useRouter();

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const { data } = await axios.post("/api/user/login", formValues);
        if (data) {
            if (!data.success) return toast.error(data.message);
            else {
                toast.success(data.message);
                setFormValues(defaultValue);
                router.push("/home");
            }
        } else {
            toast.error(data.message);
        }
    };

    return (
        <div className="bg-white px-16 pt-8 pb-12 mb-4">
            <h1 className="text-3xl mb-4 text-center">Login</h1>
            <form>
                <Input
                    type="email"
                    name="email"
                    labelText="Email"
                    value={formValues.email}
                    onChange={handleChange}
                />
                <Input
                    type="password"
                    name="password"
                    labelText="Password"
                    value={formValues.password}
                    onChange={handleChange}
                />
                <button
                    className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded-full w-full mb-4"
                    type="submit"
                    onClick={handleSubmit}
                >
                    Submit
                </button>
                <p className="text-center">
                    Dont have an account?
                    <Link
                        href="/register"
                        className="text-blue-500 hover:underline"
                    >
                        &nbsp; Register
                    </Link>
                </p>
            </form>
        </div>
    );
};

export default LoginForm;
