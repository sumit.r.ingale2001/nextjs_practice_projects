"use client";
import axios from "axios";
import { useRouter } from "next/navigation";

import toast from "react-hot-toast";

const Logout = () => {
    const router = useRouter();
    const logout = async (e) => {
        e.preventDefault();
        const { data } = await axios.get("/api/user/logout");
        if (data) {
            if (!data.success) {
                return toast.error(data.message);
            } else {
                toast.success(data.message);
                router.push("/");
            }
        } else {
            toast.error(data.message);
        }
    };

    return (
        <>
            <button
                onClick={logout}
                className="w-full bg-red-500 rounded-full px-3 py-3 uppercase text-white hover:bg-red-600"
            >
                logout
            </button>
        </>
    );
};

export default Logout;
