"use client";
import Link from "next/link";
import React, { useState } from "react";
import { Input } from "./input";
import axios from "axios";
import { toast } from "react-hot-toast";
import { useRouter } from "next/navigation";

const RegisterForm = () => {
    const defaultData = {
        username: "",
        email: "",
        password: "",
    };

    const [formValues, setFormValues] = useState(defaultData);
    const router = useRouter();

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const { data } = await axios.post("/api/user/register", formValues);
            if (data) {
                if (data.success) {
                    toast.success(data.message);
                    setFormValues(defaultData);
                    router.push("/");
                }
            }
        } catch (error) {
            toast.error(error.response.data.message);
        }
    };

    return (
        <div className="bg-white px-16 pt-8 pb-12 mb-4">
            <h1 className="text-3xl mb-4 text-center">Register</h1>
            <form>
                <Input
                    type="text"
                    name="username"
                    labelText="Username"
                    value={formValues.username}
                    onChange={handleChange}
                />
                <Input
                    type="email"
                    name="email"
                    labelText="Email"
                    value={formValues.email}
                    onChange={handleChange}
                />
                <Input
                    type="password"
                    name="password"
                    labelText="Password"
                    value={formValues.password}
                    onChange={handleChange}
                />
                <button
                    className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded-full w-full mb-4"
                    type="submit"
                    onClick={handleSubmit}
                >
                    Submit
                </button>
                <p className="text-center">
                    Already have an account?
                    <Link href="/" className="text-blue-500 hover:underline">
                        &nbsp; Login
                    </Link>
                </p>
            </form>
        </div>
    );
};

export default RegisterForm;
