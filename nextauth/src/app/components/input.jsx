export const Input = ({ name, type, labelText, value, onChange }) => (
    <>
        <div className="mb-4 flex flex-col">
            <label htmlFor={name} className="text-gray-600 font-semibold">
                {labelText}
            </label>
            <input
                type={type}
                name={name}
                autoComplete="off"
                className="w-full p-2 border border-gray-300  rounded-md focus:outline-none focus:border-blue-500 "
                onChange={onChange}
                value={value}
            />
        </div>
    </>
);
