import Connection from "@/database/db";
import User from "@/models/userSchema";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { NextResponse } from "next/server";

export const POST = async (NextRequest) => {
    try {
        await Connection();
        const data = await NextRequest.json();

        const { email, password } = data;

        if (!email || !password) {
            return new Response(
                JSON.stringify({
                    success: false,
                    message: "Feilds cannot be empty",
                }),
            );
        }

        const user = await User.findOne({ email });

        if (!user) {
            return new Response(
                JSON.stringify({
                    success: false,
                    message: "User not found",
                }),
            );
        }

        const isValid = await bcrypt.compare(password, user.password);

        if (!isValid)
            return new Response(
                JSON.stringify({
                    success: false,
                    message: "Incorrect password",
                }),
            );

        const token = jwt.sign({ id: user._id }, process.env.SECRET_KEY, {
            expiresIn: "2d",
        });

        const response = NextResponse.json({
            success: true,
            message: "Logged in successfully",
        });
        response.cookies.set("access_token", token, { httpOnly: true });
        return response;
    } catch (error) {
        return new Response(
            JSON.stringify({ success: false, message: "Error while logging" }),
            { status: 500 }
        );
    }
};
