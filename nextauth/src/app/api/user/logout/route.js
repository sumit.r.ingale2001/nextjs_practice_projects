import { NextResponse } from "next/server";

export const GET = async (NextRequest) => {
    try {
        const response = NextResponse.json({
            message: "Logout successfull",
            success: true,
        });
        response.cookies.set("access_token", "", {
            httpOnly: true,
            expires: new Date(0),
        });
        return response;
    } catch (error) {
        return new Response(
            JSON.stringify({ success: false, message: "Something went wrong" }),
            { status: 500 }
        );
    }
};
