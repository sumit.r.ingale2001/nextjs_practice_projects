import Connection from "@/database/db";
import User from "@/models/userSchema";
import bcrypt from "bcryptjs";

export const POST = async (NextRequest) => {
    try {
        await Connection();
        const body = await NextRequest.json();
        const { username, email, password } = body;

        if (!username || !username || !password) {
            return new Response(
                JSON.stringify({
                    success: false,
                    message: "Fields must not be empty",
                }),
            );
        }

        const user = await User.findOne({ email });
        if (user) {
            return new Response(
                JSON.stringify({
                    message: "User already exist",
                    success: false,
                }),
            );
        }

        const hashedPassword = await bcrypt.hash(password, 10);
        const newUser = new User({
            username,
            email,
            password: hashedPassword,
        });

        await newUser.save();
        return new Response(
            JSON.stringify({
                success: true,
                message: "Registered successfully",
            }),
            { status: 200 }
        );
    } catch (error) {
        new Response(
            JSON.stringify({
                success: false,
                message: "Error while Registering",
            }),
            {status:500}
        );
    }
};
