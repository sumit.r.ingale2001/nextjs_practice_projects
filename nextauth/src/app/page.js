import LoginForm from "./components/LoginForm";

const Home = () => {
    return (
        <>
            <div className="min-h-screen bg-gray-100 flex flex-col justify-center items-center">
                <LoginForm />
            </div>
        </>
    );
};

export default Home;

export const metadata = {
    title: "Login",
    description: "Login page of the nextAuth app",
};
